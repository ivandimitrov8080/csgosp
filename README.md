# csgosp - a CSGO server picker

### Small node script that lets you choose which CSGO server you want your next game to be on.

#### Currently works on linux only, but can be modified to work on windows too. PRs are welcome.

# Usage:

cd into the project directory and run

`pnpm i` or `npm i`

then run

`sudo pnpm run start` or `sudo npm run start`

after that pick the server you wish to play on and the script makes it possible.
