#!/usr/bin/env node

import inquirer from 'inquirer'
import { pickServer, getServerNames } from './serverPicker.js';
import { readSavedServer, saveServer } from './serverSaver.js';
import chalk from "chalk";

async function askServer() {
    console.log(chalk.red(
        "This setting will remain on your computer until you reboot or you reload your iptables."))
    const answers = await inquirer.prompt({
        name: "server",
        type: "list",
        message: chalk.green("Which server would you like to play on?"),
        default() {
            return readSavedServer()
        },
        choices: getServerNames()
    })
    return answers.server
}
const server = await askServer();

saveServer(server)

pickServer(server)
