import fs from 'fs'

const fileName = "server.sp"

export function saveServer(server) {
    fs.writeFileSync(fileName, server, (err) => {
        if (err)
            console.log(err)
    })
}

export function readSavedServer() {
    try {
        return fs.readFileSync(fileName, "utf8")
    } catch {
        return "London"
    }
}
