import chalk from "chalk";
import { drop } from '../lib/iptables.js';
import fetch from 'node-fetch';

const steamCsgoServerListUrl = "https://raw.githubusercontent.com/SteamDatabase/SteamTracking/master/Random/NetworkDatagramConfig.json"

const servers = await fetch(steamCsgoServerListUrl).then(data => data.json());

export function pickServer(server) {
    getServerList()
        .filter(serverData => serverData.desc != server)
        .forEach(dropInIptables)
}

function getServerList() {
    return Object.values(servers.pops)
        .filter(server => server.desc !== undefined)
        .filter(server => server.relays !== undefined && server.relays.length > 0)
}

function dropInIptables(serverData) {
    console.log(chalk.green(`Dropping server ${serverData.desc}`))
    for (let relay of serverData.relays) {
        drop({
            src: relay.ipv4
        })
    }
}

export function getServerNames() {
    return getServerList().map(server => server.desc)
}
